variable "prefix" {
  description = "The prefix used for all resources in this environment"
}

variable "db" {
  description = "The name of the MongoDB database"
  sensitive = true
}