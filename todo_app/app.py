from flask import Flask, render_template, request, redirect, url_for
from todo_app.flask_config import Config
from todo_app.view_model import ViewModel
from todo_app.data.mongo_items import MongoDB

def create_app():
    app = Flask(__name__)
    app.config.from_object(Config())
    mongo = MongoDB()
    # trello = Trello()

    @app.route('/')
    def index():
        items_list = mongo.get_items()
        item_view_model = ViewModel(items_list)
        return render_template('index.html', items=item_view_model.items)

    @app.route('/to_do')
    def to_do_view():
        items_list = mongo.get_items()
        item_view_model = ViewModel(items_list)
        return render_template('index.html', items=item_view_model.to_do_items)

    @app.route('/in_progress')
    def in_progress_view():
        items_list = mongo.get_items()
        item_view_model = ViewModel(items_list)
        return render_template('index.html', items=item_view_model.in_progress_items)

    @app.route('/done')
    def done_view():
        items_list = mongo.get_items()
        item_view_model = ViewModel(items_list)
        return render_template('index.html', items=item_view_model.done_items)

    @app.route('/items_list/new', methods=['POST'])
    def new_item():
        title = request.form.get('item_title')
        status = request.form.get('status')
        mongo.add_item(status, title)
        return redirect(url_for('index'))
    
    @app.route('/<item_id>/delete', methods=['POST'])
    def delete(item_id):
        mongo.delete_item(item_id)
        return redirect(url_for('index'))

    @app.route('/<item_id>/done', methods=['POST'])
    def move_to_done(item_id):
        mongo.change_status(item_id, status='Done')
        return redirect(url_for('index'))

    @app.route('/<item_id>/in_progress', methods=['POST'])
    def move_to_in_progress(item_id):
        mongo.change_status(item_id, status='In Progress')
        return redirect(url_for('index'))

    @app.route('/<item_id>/to_do', methods=['POST'])
    def move_to_to_do(item_id):
        mongo.change_status(item_id, status='To Do')
        return redirect(url_for('index'))

    return app
