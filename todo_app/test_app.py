import pytest
import mongomock
from dotenv import load_dotenv, find_dotenv
from todo_app import app
from todo_app.data.mongo_items import MongoDB

@pytest.fixture
def client():
     # Use our test integration config instead of the 'real' version
    file_path = find_dotenv('.env.test')
    load_dotenv(file_path, override=True)
    
    with mongomock.patch(servers=(('fakemongo.com', 27017),)):
        test_app = app.create_app()
        with test_app.test_client() as client:
            yield client

def test_index_page(client):
    """
    Testing that Index page will return a 200 status code and contain a card named 'Test card'
    
    """
    mongo = MongoDB()
    mongo.add_item('Test card', 'To Do')
    response = client.get('/')

    assert response.status_code == 200
    assert 'Test card' in response.data.decode()