import os
import pymongo
from todo_app.data.item import Item

class MongoDB:
    def __init__(self) -> None:
        self.db = os.getenv('MONGO_DB_NAME')
        self.connection_string = os.getenv('MONGO_DB_CONNECTION_STRING')
        self.collection = os.getenv('MONGO_DB_COLLECTION')
        self.connect_to_collection = self.connect_to_db()


    def connect_to_db(self):
        """
        Connect to the MongoDB database.
        """
        client = pymongo.MongoClient(self.connection_string)
        db = client[self.db]
        collection = db[self.collection]
        print("Connection Successful")
        return collection
    
    def get_items(self):
        """
        Get all items from the database.

        Returns
        -------
        item_list
            A list of Item objects.
        """
        item_list = []
        items = []

        for document in self.connect_to_collection.find({}):
            item_list.append(document)

        for record in item_list:
            item = Item(record['_id'], record['name'], record['status'])
            items.append(item)

        return items

    
    def change_status(self, item_id, status):
        """
        Updates the status of the selected item

        Args:
            item_id: The id of item that is being changed
            status: The new status
        """
        self.connect_to_collection.update_one({'_id': item_id}, {'$set': {'status': status}})
        
    


    def add_item(self, status, title):
        """
        Adds a new item with the specified title and status.

        Args:
            status: The status for the new item.
            title: The title of the item.

        """
        document = {'name': title, 'status': status}
        self.connect_to_collection.insert_one(document)

    def delete_item(self, item_id):
        """
        Deletes the item with the specified id.
        """
        self.connect_to_collection.delete_one({'_id': item_id})

