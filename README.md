# DevOps Apprenticeship: Project Exercise

> If you are using GitPod for the project exercise (i.e. you cannot use your local machine) then you'll want to launch a VM using the [following link](https://gitpod.io/#https://github.com/CorndelWithSoftwire/DevOps-Course-Starter). Note this VM comes pre-setup with Python & Poetry pre-installed.

## System Requirements

The project uses poetry for Python to create an isolated environment and manage package dependencies. To prepare your system, ensure you have an official distribution of Python version 3.8+ and install Poetry using one of the following commands (as instructed by the [poetry documentation](https://python-poetry.org/docs/#system-requirements)):

### Poetry installation (Bash)

```bash
curl -sSL https://install.python-poetry.org | python3 -
```

### Poetry installation (PowerShell)

```powershell
(Invoke-WebRequest -Uri https://install.python-poetry.org -UseBasicParsing).Content | py -
```

## Dependencies

The project uses a virtual environment to isolate package dependencies. To create the virtual environment and install required packages, run the following from your preferred shell:

```bash
$ poetry install
```

You'll also need to clone a new `.env` file from the `.env.template` to store local configuration options. This is a one-time operation on first setup:

```bash
$ cp .env.template .env  # (first time only)
```

The `.env` file is used by flask to set environment variables when running `flask run`. This enables things like development mode (which also enables features like hot reloading when you make a file change). There's also a [SECRET_KEY](https://flask.palletsprojects.com/en/1.1.x/config/#SECRET_KEY) variable which is used to encrypt the flask session cookie.
Ensure that the .env file is added to the .gitignore to prevent credentials from being made public.

## Running the App

Once the all dependencies have been installed, start the Flask app in development mode within the Poetry environment by running:

```bash
$ poetry run flask run
```

You should see output similar to the following:

```bash
 * Serving Flask app "app" (lazy loading)
 * Environment: development
 * Debug mode: on
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
 * Restarting with fsevents reloader
 * Debugger is active!
 * Debugger PIN: 226-556-590
```

Now visit [`http://localhost:5000/`](http://localhost:5000/) in your web browser to view the app.

## Running the tests

In order to run the tests, run the following in the terminal.

```bash
$ poetry run pytest
```

Ensure that the test files have a name of the format of test\__.py or _\_test.py.
'\*' refers to the original name of the file you want to test.
You can store the test file alongside the file you want to test.

## Docker

You'll need to install Docker Desktop (https://www.docker.com/products/docker-desktop).

#### Running the Test, Dev Prod Containers

The development container has two key behaviours:

- Enables Flask's debugging/developer mode to provide detailed logging and feedback.
- Allows rapid changes to code files without having to rebuild the image each time.

The difference between the dev, test and prod containers is that the prod container uses Gunicorn to run the app, whereas the dev container uses Flask.

You can create either a development, testing or production image from the same Dockerfile, by running the following for dev:

```bash
$ docker build --target development --tag todo-app:dev .
```

or the following for test:

```bash
$ docker build --target test --tag my-test-image .
```

or the following for prod:

```bash
$ docker build --target production --tag todo-app:prod .
```

You can then start the dev container by running:

```bash
$ docker run --env-file ./.env -p 5100:5000 --mount "type=bind,source=$(pwd)/todo_app,target=/todo_app/todo_app" todo-app:dev
```

or you can start the test container by running

```bash
$ docker run --env-file .env.test  my-test-image
```

or you can start the prod container by running:

```bash
$ docker run --env-file .env -p 5000:5000 todo-app:prod
```

## Migrating application to the Cloud (Azure)

Set up (see instructions below) or use an existing Microsoft Azure Account.

#### Setting up Microsoft Azure account

Go to the Azure portal (https://portal.azure.com) and create an account.
When prompted selected the “Start with an Azure free trial”. This gives 12 months of access to some resources, and $150 credit for 30 days, along with “always free” tiers for most common resources. Ensure you select the “FREE Tier” or “SKU F1” when creating resources, as the default is usually the cheapest paid option.
If you see an error like “The subscription you have selected already has an app with free tier enabled” then you should either delete the existing resource in the Portal, or opt for the cheap-but-paid Basic Tier for your new resource.

### Locate Resource Group

Please locate your resource group in the Azure portal and keep a note of its name; you’ll need it for the steps below.

### Install Azure CLI

Follow the instructions at: https://docs.microsoft.com/en-us/cli/azure/install-azure-cli to install the CLI on your machine if you haven’t already.
Open up new terminal window and enter

```bash
$ az login
```

which will launch a browser window to allow you to log in.

### Put Container Image on Docker Hub registry

Log into DockerHub locally, with

```bash
$ docker login
```

Build the production container image, with

```bash
$ docker build --target production --tag <docker-username>/todo-app:latest .
```

Push the production container image to Docker Hub registry, with

```bash
$ docker push <docker-username>/todo-app:latest
```


## Security
### Encryption at rest
Using Azure Cosmos DB, all the databases, media attachments and backups are encryped with the release of encryption at rest and in transport at no extra cost. Encryption at rest is 'on' as default so no extra steps need to be take to implement it. 

### Encryption in transit
Checking that the setting is turned on in the Azure Portal - you can find that under your app’s “Configuration -> General Settings” page
In addition, you should be able to see that the ssl=true query parameter is included in your CosmosDB connection string, ensuring that traffic between your application and the DB is also encrypted-in-transit by default.

### Dependancy Checking
We will use safety which is freely available for non-commercial uses to have an up-to-date understanding of the security status of any dependencies of our application. 

You can check on your dependancies locally by running the following in your terminal:
```bash
poetry run safety check
```

This will produce a simple report noting any vulnerable libraries. For the sake of this exercise it is not required to resolve all warnings, although it is obviously a good idea. This is often as simple as asking Poetry to find us a newer version:
```bash
poetry update <library>
```

## Terraform

Terraform is used as Infrastructure-as-Code (IaC) to declaratively describe our desired Azure infrastructure, and use that to deploy our todo-app with the correct arrangement of Azure resources. 

Main commands:

First run 
```bash 
terraform init
```
to initialise the directory, set up the backend and link the azurerm. 


```bash 
terraform plan
```
shows what actions would be performed to make reality match your desired configuration.

Once we start adding resources you can run 
```bash
terraform apply --var prefix="<prefix>" --var db="<db_name>"
```
which will make a new plan, ask for approval, then provision the resources. 

Curl the webhook output using the following command
```bash
curl --fail -dH -X POST "$(terraform output -raw webhook)"
```

Running 
```bash 
terraform destroy
```
will remove any resources you’ve created.

Ensure the variables listed in the variables.tf file are added to the Gitlab CI/CD pipeline as variables using the following format in order for it to be pulled into the gitlab-ci.yml file when terraform apply is run:
TF_VAR_<variable_name> 
