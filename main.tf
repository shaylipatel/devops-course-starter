terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = ">= 3.8"
    }
  }
  backend "azurerm" {
    resource_group_name  = "Cohort29_ShaPat_ProjectExercise"
    storage_account_name = "sptodoapp"
    container_name       = "sptodoappcontainer"
    key                  = "terraform.tfstate"
    }
}

provider "azurerm" {
  features {}
}

data "azurerm_resource_group" "main" {
  name     = "Cohort29_ShaPat_ProjectExercise"
}

resource "azurerm_service_plan" "main" {
  name                = "terraformed-asp"
  location            = data.azurerm_resource_group.main.location
  resource_group_name = data.azurerm_resource_group.main.name
  os_type             = "Linux"
  sku_name            = "B1"
}

resource "azurerm_linux_web_app" "main" {
  name                = "${var.prefix}-tf-todo-app"
  location            = data.azurerm_resource_group.main.location
  resource_group_name = data.azurerm_resource_group.main.name
  service_plan_id     = azurerm_service_plan.main.id

  site_config {
    application_stack {
      # docker_image_name = "appsvcsample/python-helloworld:latest"   
      docker_image_name = "shaylipatel/todo-app:latest"    
      docker_registry_url = "https://index.docker.io"
      }
  }
  app_settings = {
    "MONGO_DB_CONNECTION_STRING" = azurerm_cosmosdb_account.main.primary_mongodb_connection_string
    "FLASK_APP" = "todo_app/app"
    "SECRET_KEY" = "secret-key"
    "MONGO_DB_NAME" = "${var.db}"
    MONGO_DB_COLLECTION="todo-app-tasks"
  }
}

resource "azurerm_cosmosdb_account" "main" {
  name          = "${var.prefix}-tf-todo-data"
  location      = data.azurerm_resource_group.main.location
  resource_group_name = data.azurerm_resource_group.main.name
  offer_type          = "Standard"
  kind                = "MongoDB"
  capabilities {
      name = "EnableServerless"
  }

  geo_location {
    location          = "uksouth"
    failover_priority = 0
  }

  consistency_policy {
    consistency_level       = "Session"
  }
  capabilities { name = "EnableMongo" }

}

resource "azurerm_cosmosdb_mongo_database" "maindb" {
  name                = "${var.db}"
  resource_group_name = resource.azurerm_cosmosdb_account.main.resource_group_name
  account_name        = resource.azurerm_cosmosdb_account.main.name
  lifecycle { prevent_destroy = true }
}